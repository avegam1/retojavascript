export const easy = [
    {
      question: '¿Cuántos años tiene un lustro?',
      answers: ['A. 1', 'B. 5', 'C. 10', 'D. 50'],
      correctAnswer: 'B. 5',
      category: 'easy',
    },
    {
      question: '¿El número romano CMLV equivale a:?',
      answers: ['A. 100 505', 'B. 1155', 'C. 955', 'D. 150'],
      correctAnswer: 'C. 955',
      category: 'easy',
    },
    {
      question: '¿Dónde originaron los juegos olímpicos?',
      answers: ['A. Inglaterra', 'B. China', 'C. Estados Unidos', 'D. Grecia'],
      correctAnswer: 'D. Grecia',
      category: 'easy',
    },
    {
      question: 'Cuántos huesos hay en el cuerpo de un humano adulto?',
      answers: ['A. 206', 'B. 196', 'C. 203', 'D. 303'],
      correctAnswer: 'A. 206',
      category: 'easy',
    },
    {
      question: '¿Cuál fue el primer metal que empleó el hombre?',
      answers: ['A. Hierro', 'B. Cobre', 'C. Plata', 'D. Acero'],
      correctAnswer: 'B. Cobre',
      category: 'easy',
    },
  ]
  