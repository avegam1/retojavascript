export const intermediate = [
    {
      question: '¿Cuál es el único mamífero capaz de volar?',
      answers: ['A. Flamenco', 'B. Ardilla voladora', 'C. Marsupial', 'D. Murciélago'],
      correctAnswer: 'D. Murciélago',
      category: 'intermediate',
    },
    {
      question: '¿A qué país pertenece la ciudad de Varsovia?',
      answers: ['A. Ucrania', 'B. Moldavia', 'C. Polonia', 'D. Bielorrusia'],
      correctAnswer: 'C. Polonia',
      category: 'intermediate',
    },
    {
      question: '¿Quién compuso la mítica canción Knockin on Heavens Door?',
      answers: ['A. Guns and Roses', 'B. Avril Lavigne', 'C. Bob dylan', 'D. Eric Clapton'],
      correctAnswer: 'C. Bob dylan',
      category: 'intermediate',
    },
    {
      question: '¿Quién inventó la imprenta?',
      answers: ['A. Thomas Alva Edison', 'B. Johannes Gutenberg', 'C. Louis Lumière', 'D. Alexander Graham Bell'],
      correctAnswer: 'B. Johannes Gutenberg',
      category: 'intermediate',
    },
    {
      question: '¿Cuál es el océano más grande del mundo?',
      answers: ['A. Atlántico', 'B. Índico', 'C. Ártico', 'D. Pacífico'],
      correctAnswer: 'D. Pacífico',
      category: 'intermediate',
    },
  ]
  