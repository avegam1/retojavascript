export const veryeasy = [
    {
      question: '¿Quién es el escritor del libro Don Quijote de la Mancha?',
      answers: ['A. Miguel de cervantes', 'B. Jorge Luis Borges', 'C. Gabriel García Márquez', 'D. Julio Cortázar'],
      correctAnswer: 'A. Miguel de cervantes',
      category: 'veryeasy',
    },
    {
      question: '¿Cómo se llama el proceso por el que las plantas extraen energía de la luz solar?',
      answers: ['A. Respiración', 'B. Fotosíntesis', 'C. ototropismo', 'D. Fotorecepción"'],
      correctAnswer: 'B. Fotosíntesis',
      category: 'veryeasy',
      
    },
    {
      question: '¿Cuál es el sexto (6) planeta del sistema solar?',
      answers: ['A. Saturno', 'B. Urano', 'C. Júpiter', 'D. Neptuno'],
      correctAnswer: 'A. Saturno',
      category: 'veryeasy',

    },
    {
      question: '¿El keroseno es un derivado de?',
      answers: ['A. Petróleo', 'B. Caña de azúcar', 'C. Madera', 'D. Leche'],
      correctAnswer: 'A. Petróleo',
      category: 'veryeasy',
    },
    {
      question: 'Cómo se llama una palabra con la sílaba tónica en la última sílaba?',
      answers: ['A. Aguda', 'B. Grave', 'C. Esdrújula', 'D. Sobreesdrújula'],
      correctAnswer: 'B. Grave',
      category: 'veryeasy',
    },
  ]
  