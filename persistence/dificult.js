export const dificult = [
    {
      question: '¿En qué año llegó el ser humano a la Luna?',
      answers: ['A. 1960', 'B. 1969', ' C .1971', ' D. 1986'],
      correctAnswer: 'B. 1969',
      category: 'dificult',
    },
    {
      question: 'Cuántas teclas tiene un piano?',
      answers: ['A. 88', 'B. 95', 'C. 61', 'D. 73'],
      correctAnswer: 'A. 88',
      category: 'dificult',
    },
    {
      question: '¿Qué país tiene más islas en el mundo?',
      answers: ['A. Noruega', 'B. Indonesia', 'C. Australia', 'D. Suecia'],
      correctAnswer: 'D. Suecia',
      category: 'dificult',
    },
    {
      question: 'Quién fue la primera mujer según la mitología griega?',
      answers: ['A. Gea', 'B. Pandora', 'C. Afrodita', 'D. Psique'],
      correctAnswer: 'B. Pandora',
      category: 'dificult',
    },
    {
      question: 'Quién fue el padre de la bomba atómic?',
      answers: ['A. Tranklin Roosevelt', 'B. Robert Oppenheimer', 'C. Edward Teller', 'D. Albert Einstein'],
      correctAnswer: 'B. Robert Oppenheimer',
      category: 'dificult',
    },
  ]
  