export const verydifficult = [
    {
      question: '¿Cuál fue la primera civilización humana?',
      answers: ['A. Sumeria', 'B. Egipcia', 'C. Babilónica', 'D. China'],
      correctAnswer: 'A. Sumeria',
      category: 'verydifficult',
    },
    {
      question: '¿Dónde están ubicados los huesos escafoides?',
      answers: ['A. Rodilla', 'B. Muñeca', 'C. Cráneo', 'D. Cadera'],
      correctAnswer: 'B. Muñeca',
      category: 'verydifficult',
    },
    {
      question: '¿Qué tipo de roca es la serpentinita?',
      answers: ['A. Ígnea', 'B. Sedimentária', 'C. Metamórfica', 'D. Meteorito'],
      correctAnswer: 'C. Metamórfica',
      category: 'verydifficult',
    },
    {
      question: '¿Cuál es la flor nacional de Japón?',
      answers: ['A. Melocotón', 'B. Violeta', 'C. Crisantemo', 'D. Cerezo'],
      correctAnswer: 'D. Cerezo',
      category: 'verydifficult',
    },
    {
      question: '¿Qué representaba el dios Thot en el antiguo Egipto?',
      answers: ['A. Sol', 'B. Cielo', 'C. Sabiduría', 'D. Desierto'],
      correctAnswer: 'C. Sabiduría',
      category: 'verydifficult',
    },
  ]
  