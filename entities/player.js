/**
 * Clase jugador, se utiliza como modelo para crear cada uno de los distintos jugadores,
 * con cada uno de sus atributos y paramatros
 */
class Player {
  #nickname
  #won
  #answers
/**
 * constructor de la clase, este recibe  el atributo alias, de tipo string. 
 * @param {*String} nickname 
 */
  constructor(nickname) {
    this.#nickname = nickname
    this.#won = false
    this.#answers = []
  }
/**
 * metodo set, para modifcar el valror del atributo alias, de tipo string
 * @param {*String} nickname 
 */
  setNickname(nickname) {
    this.#nickname = nickname
  }
/**
 * metodo get, para  obtener el valor del atributo alias, y retorna el mismo.
 * @returns #nickname
 */
  getNickname() {
    return this.#nickname
  }
/**
 * metodo get, para  obtener el valor del atributo alias, y retorna el mismo.
 * @returns 
 */
  getGameResult() {
    return this.#won
  }
  /**
   * 
   */

  setPlayerVictory() {
    this.#won = true
  }
/**
 * metodo para agregar la respuesta escogida, por el jugador, recibe como parametro una pregunta de tipo string
 * @param {*string} answer 
 */
  addAnswerChosen(answer) {
    this.#answers.push(answer)
  }
/**
 * metodo para obtener la respuesta actual por nivel, recibe como parametro  un nivel de tipo string, y devuelve una respuesta segun el nivel
 * @param {*Numero} level 
 * @returns #answers[level]
 */
  getCurrentAnswerByLevel(level) {
    return this.#answers[level]
  }
}

export default Player
