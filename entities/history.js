/**
 * clase History, se utiliza como modelo para crear cada uno de las distintos  historiales del juego,
 * con cada uno de sus atributos y paramatros
 */
class History {
  #historyArray
  /**
   * constructor de la clase, inicializa  un arreglo de historial de juego, de tipo string.
   */
  constructor() {
    this.#historyArray = []
  }
/**
 * metodo get, para  obtener el  historico de juego, y retorna un metodo que permite ver el historico anterior al actual.
 * @returns readPreviousHistory()
 */
  getHistory() {
    return this.readPreviousHistory()
  }
  /**
   * metodo ushToHistor para agregar a la lista de historico un nuevo historial, necesita como parametro un juego de tipo Game
   * @param {*Game} game 
   */

  pushToHistory(game) {
    const filterData = this.extractImportantData(game)
    this.#historyArray.push(filterData)
  }
/**
 * metodo readPreviousHistory para ver  el historico de juego, este retorna un array de juego
 * @returns 
 */
  readPreviousHistory() {
    const localStoredArray = localStorage.getItem('savedData')
    if (localStoredArray) {
      const parsedArray = JSON.parse(localStoredArray)
      return parsedArray
    }
    return []
  }
/**
 * metodo  veToLocalStorage que me permite guardar  un historial de juego  de manera local en el navegador,
 * este recibe como parametro un juego.
 * @param {*Game} game 
 */
  saveToLocalStorage(game) {
    const filterData = this.extractImportantData(game)
    const currentSave = this.readPreviousHistory()
    const newSave = [...currentSave, filterData]
    const historyArray = JSON.stringify(newSave)
    localStorage.setItem('savedData', historyArray)
  }
/**
  
 * metodo extractImportantData, permite ver cual fue el resultado de la partida jugada. mediante este
 * metodo conocemos el  puntaje, alias del jugador, ronda , y si gano la partida.
 * recibe como parametro un juego de tipo juego, retorna un atributo tipo data.
 * @param {Game*} game 
 * @returns data
 */
  extractImportantData(game) {
    const score = game.showScore()
    const maxLevel = game.getCurrentLevel()
    const didWin = game.getPlayer().getGameResult()
    const nickname = game.getPlayer().getNickname()
    const data = { score, maxLevel, didWin, nickname }
    return data
  }
}
export default History
