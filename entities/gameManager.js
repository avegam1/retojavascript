import { veryeasy} from '../persistence/veryeasy.js'
import { easy  } from '../persistence/easy.js'
import { intermediate } from '../persistence/intermediate.js'
import { dificult  } from '../persistence/dificult.js'
import { verydifficult } from '../persistence/verydifficult.js'

import Display from './Display.js'
import Game from './Game.js'
import History from './History.js'
import Player from './Player.js'
import Question from './Question.js'

const questions = [
  new Question(veryeasy),
  new Question(easy ),
  new Question(intermediate),
  new Question(dificult ),
  new Question(verydifficult),
]

const singleGameInstance = new Game(questions, new Player('defaultUser'))
const singleDisplayInstance = new Display()
class GameManager {
  constructor() {}
/**
 * funcion que permite invocar la funcionalidad del boton Aceptar.
 */
  startNewGame() {
    singleDisplayInstance.nickNameScreen(singleGameInstance, this.startGameButtonCallback, this.historyButtonCallback)
  }
/**
 * funcion funcion para invocar la funcionalidad del  boton  nueva partida
 */
  startGameButtonCallback() {
    singleDisplayInstance.questionScreen(singleGameInstance)
  }
  /**
   *  funcion para invocar la funcionalidad del  boton  ver historial historico 
   */
  historyButtonCallback() {
    const history = new History()
    singleDisplayInstance.historyScreen(history)
  }
/**
 * metodo  que permite invocar la funcionalidad de pregunta y respuestas
 */
  questionScreenManager() {
    singleDisplayInstance.questionScreen(singleGameInstance)
  }
}

export default GameManager
