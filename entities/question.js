/**
 * clase Question, se utiliza como modelo para crear cada una de las distintas  preguntas que se presentaran en el juego,
 * con cada uno de sus atributos y paramatros
 */
class Question {
    #question
    /**
     * constructor de la clase, este recibe  el atributo pregunta segun el nivel, de tipo string.
     * @param {*String} questionsByLevel 
     */
    constructor(questionsByLevel) {
      this.#question = this.getRandomQuestion(questionsByLevel)
    }
  /**
   * metodo get, para  obtener el valor del atributo pregunta, y retorna el mismo.
   * @returns 
   */
    getQuestion() {
      return this.#question
    }
  /**
   * metodo get, para  obtener el titulo de la Pregunta, y retorna el mismo.
   * @returns 
   */
    getQuestionTitle() {
      return this.#question.question
    }
  
    getCorrectAnswer() {
      return this.#question.correctAnswer
    }
  /**
   * metodo get, para  obtener un conjunto de pregunta, y retorna el mismo.
   * @returns 
   */
    getAnswersArray() {
      return this.#question.answers
    }
  /**
   * metodo para  generar preguntas aleatoriamente   recibe un arreglo de tipo string
   * @param {*String} questionArray 
   * @returns questionArray
   */
    getRandomQuestion(questionArray) {
      const random = this.#generateRandomNumber(questionArray.length)
      return questionArray[random]
    }
  /**
   * metodo para crear  numeros aleatoriamente, recibe un parametro n tipo numericoy devuelve un numero aleatorio
   * @param {*numerico} n 
   * @returns 
   */
    #generateRandomNumber(n) {
      return Math.floor(Math.random() * n)
    }
  }
  
  export default Question
  