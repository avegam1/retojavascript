/**
 * clase Game, se utiliza como modelo para crear cada una de las distintas  partidas o juego del mismo,
 * con cada uno de sus atributos y paramatros.
 */
class Game {
  #currentLevel
  #questions
  #player
  #score
  /**
   * constructor de la clase, este recibe como parametros los atributos quetion y player string
   * @param {*Question} questions 
   * @param {*Player} player 
   */
  constructor(questions, player) {
    this.#currentLevel = 0
    this.#questions = questions
    this.#player = player
    this.#score = 0
  }
  /**
   * metodo validateAnswer se utiliza para validar si la respuesta escojida es  correcta,
   *  Retorna  la  respuesta escojida por el jugador
   * @param {*String} userInput 
   * @returns userAnswer
   */
  validateAnswer(userInput) {
    this.#player.addAnswerChosen(userInput)
    const userAnswer = this.#player.getCurrentAnswerByLevel(this.#currentLevel)
    const validAnswer = this.#questions[this.#currentLevel].getCorrectAnswer()
    return userAnswer === validAnswer
  }
/**
 * meotodo retireWithCurrentPoints permite  obtener los puntos actuales de la partida, retorna los puntos de la  partida en juego
 * @returns currentGameInstance
 */
  retireWithCurrentPoints() {
    const currentGameInstance = this
    return currentGameInstance
  }
/**
 * metodo continueGame() metodo que permite  incrementar el puntaje de la partida, y subir de categoria en el juego.
 */
  continueGame() {
    this.#increaseScore()
    this.#nextLevel()
  }
/**
 * metodo fixWinnerValues() permite otorgarle al ganador de la partida el maximo puntaje de la partida que son 500 puntos
 */
  fixWinnerValues() {
    this.#currentLevel = 5
    this.#score = 500
  }
/**
 * 
 */
  fixLoserLevelDisplay() {
    this.#currentLevel += 1
  }
/**
 * 
 */
  #nextLevel() {
    if (this.#currentLevel < 4) {
      this.#currentLevel += 1
    }
  }
/**
 * 
 */
  #increaseScore() {
    const INCREMENT = 100
    this.#score += INCREMENT
  }
/**
 * 
 */
  setLoserScore() {
    this.#score = 0
  }
/**
 * 
 * @returns 
 */
  getCurrentLevel() {
    return this.#currentLevel
  }/**
   * 
   * @returns 
   */
  showScore() {
    return this.#score
  }
/**
 * 
 * @returns 
 */
  getCurrentQuestion() {
    return this.#questions[this.#currentLevel]
  }
/**
 * 
 * @returns 
 */
  getQuestions() {
    return this.#questions
  }
  /**
   * 
   * @returns 
   */

  getPlayer() {
    return this.#player
  }
}

export default Game
